import React, { Fragment } from 'react';

const Cell = ({num, selected, correct, onSelect}) =>
  <div
    className={`cell${selected ? ' cell--selected' : ''}`}
    onClick={onSelect}>
    <div className="cell__inner">{num}</div>
  </div>;

const Row = ({values, selected, onSelect, name}) => {
  return values[name].values
    .map(n =>
      <Cell
        num={n}
        key={n}
        onSelect={() => onSelect(name, n)}
        selected={selected[name].includes(n)}/>
    );
};

export default function Rows({selected, onSelect, config, showResult}) {
  return <>
    <div className="row">
      <div className="row__title">Поле 1 <span className="row__title__comment">Отметьте 8 чисел.</span></div>
      <div className="row__inner">
        <Row
          onSelect={onSelect}
          values={config}
          selected={selected}
          name="first"
        />
      </div>
    </div>
    <div className="row">
      <div className="row__title">Поле 2 <span className="row__title__comment">Отметьте 1 число.</span></div>
      <div className="row__inner">
        <Row
          onSelect={onSelect}
          values={config}
          selected={selected}
          name="second"
        />
      </div>
    </div>
    <button onClick={showResult} className="show-result">Показать результат</button>
  </>
}
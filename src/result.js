import React, {Component} from 'react';
import {countIntersection, selectRandom, sort} from "./utils";
import config from './config';


export default class Result extends Component {

  state = {
    won: false,
    randomGenerated: {
      first: [],
      second: []
    },
    loading: false,
    error: null
  };


  attempts = 0;

  timeout = null;

  componentDidMount() {
    const {selected} = this.props;
    let won;

    const randomGenerated = this.generateRandom();
    const countCorrect = this.getCorrect(selected, randomGenerated);
    switch (true) {
      case countCorrect.first === 4:
      case countCorrect.first > 3 && countCorrect.second === 1:
        won = true;
        break;
      default:
        won = false;
    }

    this.setState(() => ({randomGenerated, won}));
  };

  generateRandom() {
    return {
      first: selectRandom(config.first.values, 8).sort(sort),
      second: selectRandom(config.second.values, 1).sort(sort),
    };
  }

  getCorrect(selected, generated) {
    return {
      first: countIntersection(selected.first, generated.first),
      second: countIntersection(selected.second, generated.second),
    };
  }

  sendResult = () => {
    const {selected} = this.props;
    this.setState({loading: true});
    fetch('/finch-test', {
      method: 'post',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
          selectedNumber: {
            fistField: selected.first,
            secondField: selected.second
          },
          won: this.state.won
        }
      )
    })
      .then(response => {
        if (response.ok) {
          this.setState({loading: false});
        } else {
          return Promise.reject(null);
        }
      })
      .catch(() => {
        if (this.attempts < 2) {
          this.attempts++;
          this.timeout = setTimeout(this.sendResult, 2000);
        } else {
          this.setState({error: "Ошибка при отправке данных", loading: false});
        }
      });
  };
   render() {
    const {won, randomGenerated, loading, error} = this.state;
    const {selected, reset} = this.props;

    return <div className="result">
      <div>
        { loading ? <p>Результат загружается...</p> : null}
        { error ? <p>{error}</p> : ''}
        <p>{won ? 'Ого, вы выиграли! Поздравляем!' : 'К сожалению вы не выиграли( Попробуйте еще'}</p>
        <div className="result-rows">
          <div className="row">
            <p>Выбранные номера</p>
            <div>
              <div>Поле 1</div>
              {
                selected.first.map((num) => (
                  <span
                    key={num}>
                {num}&nbsp;
              </span>
                ))
              }
            </div>
            <div>
              <div>Поле 2</div>
              {
                selected.second.map((num) => (
                  <span
                    key={num}>
                {num}
              </span>
                ))
              }
            </div>
          </div>
          <div className="row">
            <p>Выпавшие номера</p>
            <div>
              <div>Поле 1</div>
              {
                randomGenerated.first.map((num) => (
                  <span
                    key={num}>
                {num}&nbsp;
              </span>
                ))
              }
            </div>
            <div>
              <div>Поле 2</div>
              {
                randomGenerated.second.map((num) => (
                  <span
                    key={num}>
                {num}
              </span>
                ))
              }
            </div>
          </div>
        </div>
      </div>
      <button onClick={reset} className="show-result">Попробовать еще раз</button>
      <button onClick={this.sendResult} className="show-result" disabled={loading}>Отправить результат</button>
    </div>
  }
}
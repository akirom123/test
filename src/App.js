import React, {Component, Fragment} from 'react';
import './App.scss';
import Rows from "./rows";
import {MagicWind, selectRandom, sort} from "./utils";
import Result from "./result";
import config from './config';


const initialState = {
  selected: {
    first: [],
    second: []
  },
  showResult: false,
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }
  
  
  onSelect = (row, n) => {
    const {selected} = this.state;
    if (selected[row].includes(n)) {
      selected[row].splice(selected[row].indexOf(n), 1);
    } else if (selected[row].length === config[row].count) {
      return;
    } else {
      selected[row].push(n);
    }
    this.setState(() => ({selected}));
  };

  selectRandom = () => {
    this.setState(() => ({
      selected: {
        first: selectRandom(config.first.values, 8),
        second: selectRandom(config.second.values, 1),
      }
    }));
  };

  showResult = () => {
    if (this.progress() < 100) return;
    const {selected} = this.state;
    this.setState({
      selected: {
        first: selected.first.sort(sort),
        second: selected.second.sort(sort),
      },
      showResult: true,
    });
  };

  reset = () => this.setState(initialState);

  progress = () => {
    const {first, second} = this.state.selected;
    return (first.length + second.length) / config.total * 100;
  }

  render() {
    const {selected, showResult} = this.state;
    return (
      <div className="App">
        {
          showResult ?
            <Result selected={selected} reset={this.reset}/> :
            <Fragment>
              <div className="progress">
                <div className="progress__inner" style={{width: `${this.progress()}%`}}/>
              </div>
              <div className="header">
                <div className="header__title">Билет 1</div>
                <button onClick={this.selectRandom} className="header__action"><MagicWind/></button>
              </div>
              <Rows
                config={config}
                selected={selected}
                onSelect={this.onSelect}
                showResult={this.showResult}/>
            </Fragment>
        }
      </div>
    );
  }

}

export default App;

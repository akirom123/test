const config = {
  first: {
    values: Array.from(Array(19)).map((n, i) => i + 1),
    count: 8
  },
  second: {
    values: Array.from(Array(2)).map((n, i) => i + 1),
    count: 1
  },
  total: 9
};

export default config;